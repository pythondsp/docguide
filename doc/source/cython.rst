Upload Cython and Python Documents on ReadTheDoc
================================================

Please `clone the repository <https://bitbucket.org/pythondsp/docguide>`_  using following command to understand this document. Next, change the settings of the ReadTheDoc according to this tutorial to upload cython and python documents on ReadTheDoc along with autodoc features. 

.. code-block:: shell

  $ git clone https://pythondsp@bitbucket.org/pythondsp/docguide.git


Autodoc on local machine
------------------------

* First, create your cython project with setup.py file, instead of 'pyximport method'. Note that, autodoc will not be generated for cython if 'pyximport' method is used instead of setup.py file. 

* In conf.py insert the path of folders which contain python and cython files (read comments in conf.py for more details). Also, comment 'version' and 'release' lines in conf.py to prevent the display version number and release number on the document. 

  .. code-block:: python
  
      sys.path.insert(0, os.path.abspath('../../pythondsp/digcom'))
      sys.path.insert(0, os.path.abspath('../../pythondsp/meher'))

      # The short X.Y version.
      # version = '0'
      # The full version, including alpha/beta/rc tags.
      # release = '0'



Python Documentation : test.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Following is the autodoc for python files. 

.. module:: test

.. autoclass:: math2
    :members:



Cython Documentation : digcom.pyx
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is the documentation for cython file i.e. digcom.pyx. This document is saved in **digcom** folder. Note that only two classes are added in digcom.pyx i.e. SignalGenerator and TheoryBER. Therefore no link is provided for DecisionSignal and ErrorCalcualtion. 

Further, TheoryBER is not included in autosummary, therefore is not displayed in the table, but it's content is shown below the table due to automodue-digcom.

.. note::

  **Click on classes in the below table to see further details.**

.. automodule:: digcom
   
   .. rubric:: List of Classes

   .. autosummary::
        :toctree:: generated
   
        DecisionSignal
        ErrorCalcualtion
        SignalGenerator
      
   .. rubric:: Description for Classes and Methods 
   


Cython Documentation : meher.pyx
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is the documentation for cython file i.e. meher.pyx, which is saved in **meher** folder.

.. note::

  **Click on classes in the below table to see further details.**

.. automodule:: meher
   
   .. rubric:: List of Classes

   .. autosummary::
        :toctree:: generated
   
        MeherDecisionSignal
        MeherErrorCalcualtion
        MeherSignalGenerator
        MeherTheoryBER
      
   .. rubric:: Description for Classes and Methods 
   

Autodoc on ReadTheDoc
---------------------

We need to make certain settings to run autodoc on ReadTheDoc i.e. we need to add 'requirements.txt' and 'setup.py' file in main project directory. Also, some chagnes 

* Create **setup.py in main directory** which contains the location of all the cython files, as ReadTheDoc checks for setup.py on the main-project-directory only (not inside the individual folders). ReadTheDoc will execute the setup.py command by itself.
* Keep requirements.txt file in main directory. 
* Further, python files will be automatically searched by the ReadTheDoc using paths provided in the conf.py. 
* Go to advance settings on ReadTheDoc site and enable virtualenv. Also, fill the location of requirements.txt (simply write requirements.txt in the box) and select python version e.g. CPython3.x or CPython2.x.


