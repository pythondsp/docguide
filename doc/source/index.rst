.. PythonDSP documentation master file, created by
   sphinx-quickstart on Sun Jan 22 11:44:48 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Upload Cython and Python Autodoc on ReadTheDoc
==============================================

Contents:

.. toctree::
   :maxdepth: 2
   :numbered:

   cython

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

